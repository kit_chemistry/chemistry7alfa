//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [],
	timer;

//AWARDs NUMBER
var awardNum = 0;

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var minutesLeft = Math.floor(secondsLeft / 60);
		var result = "";
		secondsLeft = Math.round(secondsLeft % 60);
		
		if(minutesLeft > 9) 
			result = "" + minutesLeft + ":";
		else
			result = "0" + minutesLeft + ":";
		
		if(secondsLeft > 9)
			result += secondsLeft;
		else 
			result += "0" + secondsLeft;
		
		jqueryElement.html(result);
		
		secondsLeft += (minutesLeft * 60);
		
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}

var posAndSize = function(left, top, width, height)
{
	var pageW = 2000,
		pageH = 1000,
		leftP = left / 2000 * 100, 
		topP = top / 1000 * 100, 
		widthP = width / 2000 * 100,
		heightP = height / 1000 * 100;
		
	var css = {
		"left": leftP + "%",
		"top": topP + "%",
		"width": widthP + "%",
		"height": heightP + "%"
	};
	
	return css;
}
	
var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

var fadeOneByOne2 = function(jqueryElement, curr, interval, endFunction) //THE PREV ELEMENT FADES OUT
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			if(curr > 0)
				$(jqueryElement[curr - 1]).fadeOut(0);
			fadeOneByOne2(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function sardorAudio(currAudio, nextAudio)
{
	this.currAudio = currAudio;
	this.nextAudio = nextAudio;
	this.currAudio.addEventListener("ended", function(){
		nextAudio.play();
	});
}

sardorAudio.prototype.play = function()
{
	this.currAudio.play();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, color, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("background-color", color);
		timeout[1] = setTimeout(function(){
			jqueryElements.css("background-color", "");
			if (times) 
				blink(jqueryElements, color, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var blink2 = function(jqueryElements, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.addClass("blink-shadow");
		timeout[1] = setTimeout(function(){
			jqueryElements.removeClass("blink-shadow");
			if (times) 
				blink2(jqueryElements, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var isCorrect = function(jqueryElement)
{
	return parseInt(jqueryElement.html()) === parseInt(jqueryElement.attr("data-correct"));
}

var launch000 = function()
{
	
}

var launch101 = function()
{
	var theFrame = $("#frame-101"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		bg3 = $(prefix + ".bg-3");
	
	audio[0] = new Audio("audio/s1_1.mp3");
	audio[1] = new Audio("audio/s1_2.mp3");
	audio[2] = new Audio("audio/s1_3.mp3");
	audio[3] = new Audio("audio/s1_4.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play(); });
	audio[1].addEventListener("ended", function(){ audio[2].play(); });
	audio[2].addEventListener("ended", function(){ audio[3].play(); });
	audio[3].addEventListener("ended", function(){ 
		nextButton.fadeIn(0);
		blackSkin.fadeIn(0);
	});
	
	nextButton.fadeOut(0);
	bg1.fadeOut(0);
	bg2.fadeOut(0);
	bg3.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			bg1.fadeIn(1000);
		}, 1000);
		timeout[1] = setTimeout(function(){
			bg2.fadeIn(1000);
		}, 3000);
		timeout[2] = setTimeout(function(){
			bg3.fadeIn(1000);
		}, 7000);
		timeout[2] = setTimeout(function(){
			audio[0].play();
		}, 8000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch102 = function() {
	var theFrame = $("#frame-102"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bor = $(prefix + '.bor'),
		proton = $(prefix + '.proton'),
		neutron = $(prefix + '.neutron'),
		formula = $(prefix + '.formula'),
		formulaTwo = $(prefix + '.formula-two');
		numberTwo = $(prefix + '.number-two'),
		numberTwoLight = $(prefix + '.number-two-light'),
		numberThree = $(prefix + '.number-three'),
		numberThreeLight = $(prefix + '.number-three-light'),
		numberFive = $(prefix + '.number-five'),
		numberFiveLight = $(prefix + '.number-five-light'),
		nucleus = $(prefix + '.nucleus'),
		levels = $(prefix + '.levels'),
		twoElectrons = $(prefix + '.two-electrons'),
		threeElectrons =  $(prefix + '.three-electrons') ;

	nextButton.fadeOut(0);
	formula.fadeOut(0);
	formulaTwo.fadeOut(0);
	nucleus.fadeOut(0);	
	numberTwoLight.fadeOut(0);
	numberThreeLight.fadeOut(0);
	numberFiveLight.fadeOut(0);
	twoElectrons.fadeOut(0);

	audio[0] = new Audio("audio/s1_5.mp3");
	audio[1] = new Audio("audio/s1_6_1.mp3");
	audio[2] = new Audio("audio/s1_6_2.mp3");
	audio[3] = new Audio("audio/s1_6_3.mp3");


	audio[4] = new Audio("audio/s1_7.mp3");
	audio[5] = new Audio("audio/s1_8.mp3");
	audio[6] = new Audio("audio/s1_9.mp3");		

	var protonSprite = new Motio(proton[0], {
		"fps": "3", 
		"frames": "12"
	});

	protonSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			setTimeout(function() {
				numberTwoLight.fadeIn(0);
				numberTwo.fadeOut(0);	
				audio[3].play();
			},500);
		}
	});

	var neutronSprite = new Motio(neutron[0], {
		"fps": "3", 
		"frames": "11"
	});
	
	neutronSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			nucleus.fadeIn(0);
			setTimeout(function() {
				audio[4].play();
				numberTwoLight.fadeOut(0);
				numberTwo.fadeIn(0);
				levels.show();	
				levelsSprite.play();
			},1000)
		}
	});

	var levelsSprite = new Motio(levels[0], {
		"fps": "3", 
		"frames": "9"
	});

	levelsSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();			
		}
	});

	var threeElectronsSprite = new Motio(threeElectrons[0], {
		"fps": "3", 
		"frames": "9"
	});

	threeElectronsSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();			
		}
	});

	neutron.hide();
	levels.hide();
	threeElectrons.hide();
	proton.hide();

	audio[0].addEventListener("ended", function(){
		setTimeout(function() {
			numberFiveLight.fadeIn(0);
			numberFive.fadeOut(0);
			audio[1].play();
		},500)	
	});

	audio[1].addEventListener("ended", function(){
		setTimeout(function() {
			audio[2].play();
			proton.show();
			protonSprite.play();
			numberFiveLight.fadeOut(0);
			numberFive.fadeIn(0);
		},500)	
	});

	audio[3].addEventListener("ended", function(){	
		 formula.fadeIn(0);
		 setTimeout(function() {
		 	formulaTwo.fadeIn(0);
		 	setTimeout(function() {
		 		neutron.show();
				neutronSprite.play();
		 	},1000)
		 },1000)
	});

	audio[4].addEventListener("ended", function() {
		twoElectrons.fadeIn(0);
		numberThree.fadeOut(0);
		numberThreeLight.fadeIn(0);

		setTimeout(function() {	
			numberThree.fadeIn(0);
			numberThreeLight.fadeOut(0);		
			audio[5].play();
			threeElectrons.show();	
			threeElectronsSprite.play();
		}, 1000)
	});

	audio[5].addEventListener("ended", function(){				
		audio[6].play();
	});

	audio[6].addEventListener("ended", function() {
		setTimeout(function() {
			nextButton.fadeIn(0);
			blackSkin.fadeIn(0);
		},3000)

	});

	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bor.addClass("box-shadow-yellow-big");
		setTimeout(function() {
			bor.removeClass("box-shadow-yellow-big");
		},5000);
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);	
}

var launch103 = function() {
	var theFrame = $("#frame-103"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/s2_1.mp3");
	
	audio[0].addEventListener("ended", function(){
		nextButton.fadeIn(1000);
		blackSkin.fadeIn(1000);
	});
	
	nextButton.fadeOut(0);
	bg1.fadeOut(0);
	bg2.fadeOut(0);
	cloud.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg1.fadeIn(1000);
		bg2.fadeIn(1000);
		
		timeout[0] = setTimeout(function(){
			bg2.css("background-size", "100%");
		}, 1000);
		timeout[1] = setTimeout(function(){
			cloud.fadeIn(1000);
			audio[0].play();
		}, 4000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch104 = function() {
	var theFrame = $("#frame-104"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg = $(prefix + ".bg");
	
	audio[0] = new Audio("audio/s3_1.mp3");
	audio[1] = new Audio("audio/s3_2.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play(); });	
	audio[1].addEventListener("ended", function(){ 
		timeout[0] = setTimeout(function(){
			nextButton.fadeIn(1000);
			blackSkin.fadeIn(1000);
		}, 2000);
	});
	
	nextButton.fadeOut(0);
	bg.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg.fadeIn(500);
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch105 = function() {
	var theFrame = $("#frame-105"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		bg3 = $(prefix + ".bg-3"), 
		bg4 = $(prefix + ".bg-4"), 
		bulb = $(prefix + ".bulb"),
		scheme = $(prefix + ".scheme");
	
	schemeSprite = new Motio(scheme[0], {
		"frames": "6",
		"fps": "0.3"
	});
	schemeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[5] = setTimeout(function(){
				nextButton.fadeIn(0);
				blackSkin.fadeIn(0);
			}, 3000);
		}
	});
	
	audio[0] = new Audio("audio/ding-sound.mp3");
	audio[1] = new Audio("audio/s4_1.mp3");
	audio[2] = new Audio("audio/s4_2.mp3");
	audio[3] = new Audio("audio/s4_3.mp3");
	audio[4] = new Audio("audio/s4_4.mp3");
	
	audio[1].addEventListener("ended", function(){ audio[2].play(); });	
	audio[2].addEventListener("ended", function(){ audio[3].play();	});
	audio[3].addEventListener("ended", function(){ audio[4].play();	});
	
	nextButton.fadeOut(0);
	bg1.fadeOut(0);
	bg2.fadeOut(0);
	bg3.fadeOut(0);
	bg4.fadeOut(0);
	bulb.fadeOut(0);
	scheme.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg1.fadeIn(500);
		bg2.fadeIn(500);
		
		timeout[0] = setTimeout(function(){
			audio[0].play();
			bg2.fadeOut(0);
			bg3.fadeIn(0);
			bulb.fadeIn(0);
		}, 2000);
		timeout[2] = setTimeout(function(){
			audio[1].play();
		}, 3000);
		timeout[3] = setTimeout(function(){
			bg4.fadeIn(500);
			scheme.fadeIn(500);
		}, 4000);
		timeout[4] = setTimeout(function(){
			schemeSprite.play();
		}, 6000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch106 = function() {
	var theFrame = $("#frame-106"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg = $(prefix + ".bg"),
		els = $(prefix + ".el");
	
	audio[0] = new Audio("audio/s5_1.mp3");
	audio[1] = new Audio("audio/s5_2.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play(); });
	
	nextButton.fadeOut(0);
	els.fadeOut(0);
	bg.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg.fadeIn(500);
		audio[0].play();
		fadeOneByOne2(els, 0, 2500, function(){
			timeout[0] = setTimeout(function(){
				nextButton.fadeIn(500);
				blackSkin.fadeIn(500);
			}, 2000);
		});
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107 = function() {
	var theFrame = $("#frame-107"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-frame-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg = $(prefix + ".bg"),
		anims = $(prefix + ".anim"),
		filling = $(prefix + ".filling"),
		rollButton = $(prefix + ".roll-button"),
		roll = $(prefix + ".roll");
	
	audio[0] = new Audio("audio/s6_1.mp3");
	audio[1] = new Audio("audio/s6_2.mp3");
	audio[2] = new Audio("audio/s6_3.mp3");
	audio[3] = new Audio("audio/s6_4.mp3");
	audio[4] = new Audio("audio/s6_5.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play(); });
	audio[1].addEventListener("ended", function(){ audio[2].play(); });
	audio[2].addEventListener("ended", function(){ audio[3].play(); });
	audio[3].addEventListener("ended", function(){ audio[4].play(); });
	audio[4].addEventListener("ended", function(){ 
		
	});
	
	var rollButtonListener = function(){
		roll.fadeToggle(500);
	};
	rollButton.off("click", rollButtonListener);
	rollButton.on("click", rollButtonListener);
	
	nextButton.fadeOut(0);
	anims.fadeOut(0);
	bg.fadeOut(0);
	filling.fadeOut(0);
	rollButton.fadeOut(0);
	roll.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg.fadeIn(1000);
		audio[0].play();
		timeout[0] = setTimeout(function(){
			$(anims[0]).fadeIn(0);
		}, 4500);
		timeout[1] = setTimeout(function(){
			$(anims[0]).fadeOut(0);
			$(anims[1]).fadeIn(0);
		}, 7000);
		timeout[2] = setTimeout(function(){
			$(anims[1]).fadeOut(0);
			$(anims[2]).fadeIn(0);
		}, 18000);
		timeout[3] = setTimeout(function(){
			$(anims[2]).fadeOut(0);
			$(anims[3]).fadeIn(0);
		}, 18500);
		timeout[4] = setTimeout(function(){
			$(anims[3]).fadeOut(0);
			$(anims[4]).fadeIn(0);
		}, 19000);
		timeout[5] = setTimeout(function(){
			$(anims[4]).fadeOut(0);
			$(anims[5]).fadeIn(0);
		}, 19500);
		timeout[6] = setTimeout(function(){
			$(anims[5]).fadeOut(0);
			$(anims[6]).fadeIn(0);
		}, 21000);
		timeout[7] = setTimeout(function(){
			$(anims[7]).fadeIn(0);
		}, 22000);
		timeout[8] = setTimeout(function(){
			$(anims[6]).fadeOut(0);
			$(anims[7]).fadeOut(0);
			$(anims[8]).fadeIn(0);
		}, 32000);
		timeout[9] = setTimeout(function(){
			$(anims[8]).fadeOut(0);
			filling.fadeIn(0);
			rollButton.fadeIn(1000);
			nextButton.fadeIn(1000);
		}, 34000);
	};	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch108 = function() {
	var theFrame = $("#frame-108"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		keepThinking = $(prefix + ".keep-thinking"),
		elems = $(prefix + ".elem");
	
	audio[0] = new Audio("audio/s7_1.mp3");
	audio[1] = new Audio("audio/s7_2.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play();	})
	audio[1].addEventListener("ended", function(){ 
	})
	
	nextButton.fadeOut(0);
	keepThinking.fadeOut(0);
	elems.fadeOut(0);
	blackSkin.fadeIn(0);
	startButton.fadeIn(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		fadeOneByOne(elems, 0, 1000, function(){
			blackSkin.fadeIn(500);
			keepThinking.fadeIn(500);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(500);
				blackSkin.fadeOut(500);
		}, 3000);
		});
	};	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch109 = function() {
	var theFrame = $("#frame-109"),
		prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-frame-button"),
		newElementButton = $(prefix + ".new-element-button"),
		ballContainer = $(prefix + ".ball-container"),
		bgs = $(prefix + ".bg"),
		bgBase = $(prefix + ".bg-base"), 
		ball = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		electrons = $(prefix + ".electron"),
		trueFalse = $(prefix + ".true-false"),
		s1 = $(prefix + ".s1"),
		s2 = $(prefix + ".s2"),
		s3 = $(prefix + ".s3"), 
		s4 = $(prefix + ".s4"),
		p2 = $(prefix + ".p2"),
		p3 = $(prefix + ".p3");
	
	audio[0] = new Audio("audio/s9_1.mp3");
	audio[1] = new Audio("audio/s9_2.mp3");
	audio[2] = new Audio("audio/s9_3.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play();	})
	audio[1].addEventListener("ended", function(){ audio[2].play();	})
	audio[2].addEventListener("ended", function(){	})
	
	s1Sprite = new Motio(s1[0], {
		"frames": "2"
	});
	s2Sprite = new Motio(s2[0], {
		"frames": "2"
	});
	s3Sprite = new Motio(s3[0], {
		"frames": "2"
	});
	s4Sprite = new Motio(s4[0], {
		"frames": "2"
	});
	p2Sprite = new Motio(p2[0], {
		"frames": "6"
	});
	p3Sprite = new Motio(p3[0], {
		"frames": "6"
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	var successFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		var veg = vegetable.clone();
		basket.append(veg);
		basket.attr("data-key") === "up" ? basket.attr("data-key", "down") : basket.attr("data-key", "done")
		
		var clName = basket.attr("data-electron");
		switch(clName)
		{
			case "s1": s1.css("display") === "none" ? s1.fadeIn(0) : s1Sprite.to(s1Sprite.frame + 1, true); break;
			case "s2": s2.css("display") === "none" ? s2.fadeIn(0) : s2Sprite.to(s2Sprite.frame + 1, true); break;
			case "s3": s3.css("display") === "none" ? s3.fadeIn(0) : s3Sprite.to(s3Sprite.frame + 1, true); break;
			case "s4": s4.css("display") === "none" ? s4.fadeIn(0) : s4Sprite.to(s4Sprite.frame + 1, true); break;
			case "p2": p2.css("display") === "none" ? p2.fadeIn(0) : p2Sprite.to(p2Sprite.frame + 1, true); break;
			case "p3": p3.css("display") === "none" ? p3.fadeIn(0) : p3Sprite.to(p3Sprite.frame + 1, true); break;
		}
		
		if(basket.attr("data-key") === "done")
			basket.append("<div class='point'></div>");
	};
	var failFunction = function(vegetable, basket){
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	var finishCondition = function(){
		for(var i = 0; i < baskets.length; i++)
			if ($(baskets[i]).attr("data-key") !== "done")
				return false;
		return true;
	};
	var finishFunction = function(){
		nextButton.fadeIn(500);
		newElementButton.fadeIn(500);
	};
	
	var dragTask = new DragTask(ball, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	electrons.fadeOut(0);
	nextButton.fadeOut(0);
	newElementButton.fadeOut(0);
	trueFalse.fadeOut(0);
	timeout[0] = setTimeout(function(){
		audio[0].play();
		blink2(ballContainer, 1000, 3, function(){
			blink2(baskets, 1000, 3, function(){
				trueFalse.fadeIn(500);
				timeout[1] = setTimeout(function(){
					trueFalse.fadeOut(500);
				}, 3000);
			});
		});
	}, 2000);
}

var launch114 = function() {
	var theFrame = $("#frame-114"),
		prefix = "#" + theFrame.attr("id") + " ",
		blackSkin = $(prefix + ".black-skin"),
		startButton = $(prefix + ".start-button"),
		arrows = $(prefix + ".arrow"),
		buttons = $(prefix + ".button"),
		sketchpadContainer = $(prefix + ".sketchpad-container"),
		sketchpadJQ = $(prefix + "#responsive-sketchpad"),
		keepThinking = $(prefix + ".keep-thinking"), 
		newPicButton = $(prefix + ".new-pic-button"),
		nextFrameButton = $(prefix + ".next-frame-button");
	
	audio[0] = new Audio("audio/s12_1.mp3");
	audio[1] = new Audio("audio/s12_2.mp3");
	
	audio[0].addEventListener("ended", function(){ 
		audio[1].play();	
		blink2(sketchpadJQ, 1000, 1, function(){
			blink2(buttons, 1000, 1, function(){
				blackSkin.fadeIn(0);
				keepThinking.fadeIn(500);
				timeout[0] = setTimeout(function(){
					blackSkin.fadeOut(500);
					keepThinking.fadeOut(500);
				}, 1000);
			});
		});
	});
	audio[1].addEventListener("ended", function(){ 
			
	});
	
	var sketchpad = sketchpadJQ.sketchpad(
	{
		aspectRatio: 5/3            // (Required) To preserve the drawing, an aspect ratio must be specified
	});	
	
	sketchpad.setLineColor('#177EE5');
	sketchpad.setLineSize(3);
	
	$(".sketch-control").click(function(){
		switch($(this).attr("id"))
		{
			case "clear":
				sketchpad.clear();
				break;
			case"undo":
				sketchpad.undo();
				break;
			case "red":
				sketchpad.setLineColor('#D5000D');
				break;
			case "blue":
				sketchpad.setLineColor('#177EE5');
				break;
			case "download":
				var canvas = document.getElementById("responsive-sketchpad");
				var context = canvas.getContext("2d");
				context.fillStyle = "green";
				window.open(canvas.toDataURL("image/png"), '_blank');
				newPicButton.fadeIn(500);
				nextFrameButton.fadeIn(500);
				break;
		}
	});
	
	arrows.fadeOut(0);
	keepThinking.fadeOut(0);
	newPicButton.fadeOut(0);
	nextFrameButton.fadeOut(0);
	
	var newPicButtonListener = function(){
		sketchpad.clear();
		newPicButton.fadeOut(500);
		nextFrameButton.fadeOut(500);
		arrows.fadeOut(500);
	}
	newPicButton.off("click", newPicButtonListener);
	newPicButton.on("click", newPicButtonListener);
	
	var buttonListener = function(){
		arrows.fadeOut(0);
		var classname = $(this).attr("data-key");
		$("." + classname).fadeIn(500);
	}
	buttons.off("click", buttonListener);
	buttons.on("click", buttonListener);	
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch115 = function() {
	var theFrame = $("#frame-115"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		boyThinking = $(prefix + ".boy-thinking"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/s14_1.mp3");
	audio[1] = new Audio("audio/s14_2.mp3");
	audio[2] = new Audio("audio/s14_3.mp3");
	
	audio[0].addEventListener("ended", function(){ audio[1].play(); });
	audio[1].addEventListener("ended", function(){ audio[2].play(); });
	audio[2].addEventListener("ended", function(){ 
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	});
	
	nextButton.fadeOut(0);
	boyThinking.fadeOut(0);
	cloud.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boyThinking.fadeIn(500);
			cloud.fadeIn(500);
			audio[0].play();
		}, 2000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch116 = function() {
	var theFrame = $("#frame-116"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg = $(prefix + ".bg-base"),
		zoomIn = $(prefix + ".zoom-in"),
		aiO = $(prefix + ".ai-o");
		
	for(var i = 0; i < 13; i++)
		audio[i] = new Audio("audio/s15_" + (i + 1) + ".mp3");
	
	var sardorAudios = [];
	
	for(var j = 0; j < 12; j++)
	{	
		sardorAudios.push(new sardorAudio(audio[j], audio[j+1]));
	}
	
	audio[12].addEventListener("ended", function(){
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	});
	
	var aiOSprite = new Motio(aiO[0], {
		"fps": "1",
		"frames": "7"
	});
	aiOSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	nextButton.fadeOut(0);
	bg.fadeOut(0);
	zoomIn.fadeOut(0);
	aiO.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		bg.fadeIn(1000);
		sardorAudios[0].play();
		timeout[0] = setTimeout(function(){
			zoomIn.fadeIn(1000);
		}, 3000);
		timeout[1] = setTimeout(function(){
			aiO.fadeIn(500);
		}, 38000);
		timeout[2] = setTimeout(function(){
			aiOSprite.play();
		}, 40000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch117 = function() {
	var theFrame = $("#frame-117"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"), 
		arrows = $(prefix + ".arrow"),
		baskets = $(prefix + ".basket"),
		upArrows = $(prefix + ".up-arrow"),
		downArrows = $(prefix + ".down-arrow"),
		elements = $(prefix + ".element"),
		keepThinking = $(prefix + ".keep-thinking"),
		result = $(prefix + ".result"),
		infos = $(prefix + ".info"),
		timer = $(prefix + ".timer"),
		currPair = 1; //currently active pair

	for (var i = 0; i < 4; i++)
		audio[i] = new Audio("audio/s16_" + (i+1) + ".mp3");
	
	sAudio = [];
	
	for(var i = 0; i < 3; i++)
		sAudio[i] = new sardorAudio(audio[i], audio[i+1]);
	
	audio[3].addEventListener("ended", function(){
		startTimer(timer, 120, function(){
			blackSkin.fadeIn(0);
			result.fadeIn(0);
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				nextButton.fadeIn(0);
			}, 2000);
		});
	});
	
	nextButton.fadeOut(0);
	keepThinking.fadeOut(0);
	result.fadeOut(0);
	elements.fadeOut(0);
	
	var arrowListener = function(){
		var classname = $(this).attr("data-info"),
			info = $("." + classname),
			num = parseInt(info.html()),
			inc = 0;
			
		$(this).hasClass("up-arrow") ? inc = 1 : inc = -1;
		
		num += inc;
		
		num <= 0 ? info.html(num) : info.html("+" + num);
		
		if(num === parseInt(info.attr("data-correct")))
			info.css("background-image", "url(pics/s17-grey.png)");
		else
			info.css("background-image", "");
		
		var allCorrect = true;
		for(var i = 0; i < infos.length; i++)
			if(!isCorrect($(infos[i])))
				allCorrect = false;
			
		if(allCorrect)
		{
			blackSkin.fadeIn(500);
			keepThinking.fadeIn(500);
			
			timeout[0] = setTimeout(function(){
				blackSkin.fadeOut(500);
				keepThinking.fadeOut(500);
			}, 2000);
			
			infos.html(""+0);
			infos.css("background-image", "");
			baskets.css({
				"font-size": "",
				"text-align": ""
			});
			baskets.html("Перетяни сюда символ элемента");
			currPair++;
			if(currPair <= 10)
				setCorrectAnswers();
			else
			{
				stopTimer();
				keepThinking.fadeOut(0);
				result.fadeIn(0);
				timeout[1] = setTimeout(function(){
					hideEverythingBut($("#frame-118"));
				}, 5000);
			}
		}
	}
	arrows.off("click", arrowListener);
	arrows.on("click", arrowListener);
	
	var setCorrectAnswers = function(){
		$(".charge-1").attr("data-correct", $(".pair-"+currPair+".element-1").attr("data-charge"));
		$(".index-1").attr("data-correct", $(".pair-"+currPair+".element-1").attr("data-index"));
		
		$(".charge-2").attr("data-correct", $(".pair-"+currPair+".element-2").attr("data-charge"));
		$(".index-2").attr("data-correct", $(".pair-"+currPair+".element-2").attr("data-index"));
		
		elements.fadeOut(0);
		$(".pair-" + currPair).fadeIn(0);
	}
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		
		setCorrectAnswers();
		sAudio[0].play();
		timeout[4] = setTimeout(function(){
			blink2(elements, 1000, 2, function(){});
		}, 4000);
		
		timeout[5] = setTimeout(function(){
			blink2(infos, 1000, 2, function(){});
		}, 13000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var successCondition = function(vegetable, basket){
		return basket === "basket";
	}
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.attr("data-key"));
		basket.css({
			"font-size": "5vmax",
			"text-align": "center"
		});
		vegetable.css({
			"left": "",
			"top": "",
			"z-index": ""
		});
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"left": "",
			"top": "",
			"z-index": ""
		});
	}
	
	var finishCondition = function(){}
	
	var finishFunction = function(){}
	
	var dragTask = new DragTask(elements, successCondition, successFunction, failFunction, finishCondition, finishFunction);
}

var launch118 = function() {
	var theFrame = $("#frame-118"),
		prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		plane = $(prefix + ".plane");
	
	audio[0] = new Audio("audio/s17_1.mp3");
	
	nextButton.fadeOut(0);
	blackSkin.fadeOut(0);
	plane.fadeOut(0);
	
	audio[0].play();	
	timeout[0] = setTimeout(function(){
		plane.fadeIn(500);
		plane.css("top", "20%");
		timeout[1] = setTimeout(function(){
			plane.addClass("plane-2");
		}, 4000);
		timeout[2] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 8000);
	}, 2000);
}

var launch119 = function() {
	var theFrame = $("#frame-119"),
		prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg = $(prefix + ".bg-base"),
		boy1 = $(prefix + ".boy-1"), 
		boy2 = $(prefix + ".boy-2"), 
		bulb = $(prefix + ".bulb");
	
	var bulbSound = new Audio("audio/ding-sound.mp3");
	
	for (var i = 0; i < 6; i++)
		audio[i] = new Audio("audio/s18_" + (i+1) + ".mp3");
	
	sAudio = [];
	
	for(var i = 0; i < 5; i++)
		sAudio[i] = new sardorAudio(audio[i], audio[i+1]);
	
	audio[5].addEventListener("ended", function(){
		timeout[4] = setTimeout(function(){
			nextButton.fadeIn(500);
			blackSkin.fadeIn(500);
		}, 2000);
	});
	
	blackSkin.fadeOut(0);
	nextButton.fadeOut(0);
	bg.fadeOut(0);
	boy1.fadeOut(0);
	boy2.fadeOut(0);
	bulb.fadeOut(0);
	
	timeout[0] = setTimeout(function(){
		bg.fadeIn(0);
		boy1.fadeIn(0);
	}, 1000);
	timeout[0] = setTimeout(function(){
		bulbSound.play();
		boy1.fadeOut(0);
		boy2.fadeIn(0);
		bulb.fadeIn(0);
	}, 2000);
	timeout[1] = setTimeout(function(){
		sAudio[0].play();
	}, 4000);
}

var launch201 = function() {
	
}

var launch202 = function() {
	var theFrame = $("#frame-202"),
		prefix = "#" + theFrame.attr("id") + " ",
		blackSkin = $(prefix + ".black-skin"),
		result = $(prefix + ".result"),
		nextButton = $(prefix + ".next-button"),
		turbine = $(prefix + ".turbine"),
		turbineButtons = $(prefix + ".turbine-button"),
		tasks = $(prefix + ".task"),
		currentTask = 0,
		answers = $(prefix + ".answer"), 
		awardLabel = $(prefix + ".award .label");
		
	var nextTask = function(){
		if(currentTask === 10)
		{
			blackSkin.fadeIn(500);
			result.html("У вас " + awardLabel.html() + " правильных ответов.");
			result.fadeIn(500);
			timeout[7] = setTimeout(function(){
				result.fadeOut(0);
				nextButton.fadeIn(500);
			}, 4000);
		}
		else
		{
			currTask = $(".task-" + currentTask);
			currTask.css("left", "150%");
			timeout[1] = setTimeout(function(){
				tasks.fadeOut(0);
				currTask.css("left", "");
				currTask = $(".task-" + (++currentTask));
				currTask.css("left", "-50%");
			}, 2000);
			timeout[2] = setTimeout(function(){
				currTask.fadeIn(0);
				currTask.css("left", "");
			}, 3000);
			timeout[3] = setTimeout(function(){
				$(".task-"+currentTask+".elname").css("opacity", "1");
			}, 5000);
		}
	}
		
	var turbineButtonsListener = function(){
		nextTask();
	}
	turbineButtons.off("click", turbineButtonsListener);
	turbineButtons.on("click", turbineButtonsListener);
	
	var answerListener = function(){
		currAnswer = $(this);
		
		if(currAnswer.attr("data-correct"))
		{
			if(currAnswer.hasClass("answer-1"))
				currAnswer.css("background-image", "url(pics/test-cloud-1-green.png)");
			if(currAnswer.hasClass("answer-2"))
				currAnswer.css("background-image", "url(pics/test-cloud-2-green.png)");
			if(currAnswer.hasClass("answer-3"))
				currAnswer.css("background-image", "url(pics/test-cloud-3-green.png)");
			
			var planesNum = parseInt(awardLabel.html());
			awardLabel.html(++planesNum);
		}
		else
		{
			if(currAnswer.hasClass("answer-1"))
				currAnswer.css("background-image", "url(pics/test-cloud-1-red.png)");
			if(currAnswer.hasClass("answer-2"))
				currAnswer.css("background-image", "url(pics/test-cloud-2-red.png)");
			if(currAnswer.hasClass("answer-3"))
				currAnswer.css("background-image", "url(pics/test-cloud-3-red.png)");
			
			var correct,
				currAnswers = $(prefix + ".task-" + currentTask + ".answer")
			
			for(var i = 0; i < currAnswers.length; i++)
				if($(currAnswers[i]).attr("data-correct"))
					correct = $(currAnswers[i]);
				
			if(correct.hasClass("answer-1"))
				correct.css("background-image", "url(pics/test-cloud-1-green.png)");
			if(correct.hasClass("answer-2"))
				correct.css("background-image", "url(pics/test-cloud-2-green.png)");
			if(correct.hasClass("answer-3"))
				correct.css("background-image", "url(pics/test-cloud-3-green.png)");
		}
	}
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	tasks.fadeOut(0);
	blackSkin.fadeOut(0);
	result.fadeOut(0);
	nextButton.fadeOut(0);
	$(".task-" + currentTask).fadeIn(0);
	turbine.fadeOut(0);
	
	timeout[0] = setTimeout(function(){
		turbine.fadeIn(500);
	}, 3000);
}

var launch301 = function() {
	var theFrame = $("#frame-301"),
		prefix = "#" + theFrame.attr("id") + " ",
		answer = $(prefix + ".answer"),
		question = $(prefix + ".question"),
		checkButton = $(prefix + ".check-button"),
		backButton = $(prefix + ".back-button");
		
	var checkButtonListener = function(){
		answer.fadeIn(500);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	answer.fadeOut(0);
	question.fadeOut(0);
	
	timeout[0] = setTimeout(function(){
		question.fadeIn(500);
	}, 2000);
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			launch000();
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			launch105();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-107":
			launch107();
			break;
		case "frame-108":
			launch108();
			break;
		case "frame-109":
			launch109();
			break;
		case "frame-114":
			launch114();
			break;
		case "frame-115":
			launch115();
			break;
		case "frame-116":
			launch116();
			break;
		case "frame-117":
			launch117();
			break;
		case "frame-118":
			launch118();
			break;
		case "frame-119":
			launch119();
			break;
		case "frame-201":
			launch201();
			break;
		case "frame-202":
			launch202();
			break;
		case "frame-301":
			launch301();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);